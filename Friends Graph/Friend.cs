﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace Friends_Graph
{

  class Friend
  {

    private string Email = "";

    private ICollection<Friend> FriendList = new List<Friend>();
    public Friend(string email) {
      Email = email;
    }

    public void AddFriend(Friend friend) {
      FriendList.Add(friend);
    }

    public bool isFriend(Friend friend, List<Friend> visited = null) {
      visited = visited ?? new List<Friend>();
      if (FriendList.Contains(friend)) {
        return true;
      } else {
        foreach (Friend checkFriend in this.FriendList) {
          if (visited.Contains(checkFriend)) {
            continue;
          } else {
            visited.Add(checkFriend);
            bool check = checkFriend.isFriend(friend, visited);
            if (check) return true;
          }
        }
      }
      return false;
    }
  }
}