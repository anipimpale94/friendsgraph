﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Friends_Graph
{
  class Program
  {
    static void Main(string[] args) {

      // Friend Graph Problem Main 
      Friend A = new Friend("A");
      Friend B = new Friend("B");
      Friend C = new Friend("C");
      Friend D = new Friend("D");
      Friend E = new Friend("E");

      A.AddFriend(B);
      B.AddFriend(C);
      B.AddFriend(D);
      D.AddFriend(C);
      D.AddFriend(E);

      Console.WriteLine(A.isFriend(E));
      Console.ReadLine();
    }
  }
}
